package cl.palacios.www.ratontest1.Entity;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import cl.palacios.www.ratontest1.Database.Database;

@Table(database = Database.class) //Esta linea es la que permite al DBFlob generar las tablas
public class Usuario extends BaseModel {

    @Column
    @PrimaryKey(autoincrement = true)
    private int id;

    @Column
    private String nombre;

    @Column
    private String apellido;

    @Column
    private String correo;

    @Column
    private String user;

    @Column
    private String pass;

    @Column
    private boolean usuario_activo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isUsuario_activo() {
        return usuario_activo;
    }

    public void setUsuario_activo(boolean usuario_activo) {
        this.usuario_activo = usuario_activo;
    }

    public static Usuario getUsuario(String usuario){

        //Ejemplo de metodo de busqueda en tabla usuario por el email
        //

        Usuario usr;

        usr = SQLite.select()
                .from(Usuario.class)
                .where(Usuario_Table.correo.eq(usuario)) //El Usuario_Table lo auto genera el DBFlow
                .querySingle();                         //Si no se ha generado debes hacer un build

        if (usr!=null){
            return usr;
        }else {
            return new Usuario();
        }

    }
}
