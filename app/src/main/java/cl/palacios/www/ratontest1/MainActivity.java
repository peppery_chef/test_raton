package cl.palacios.www.ratontest1;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.raizlabs.android.dbflow.config.FlowConfig;
import com.raizlabs.android.dbflow.config.FlowManager;

import cl.palacios.www.ratontest1.Entity.Usuario;

public class MainActivity extends AppCompatActivity {

    private EditText usuario;
    private EditText contrasena;
    private Button btnGuardar;
    private Button btnBuscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Se debe Iniciar el DBFlow junto con el primer activity de la aplicación
        //Se inicia con la siguiente Linea
        FlowManager.init(new FlowConfig.Builder(this).build());

        usuario = findViewById(R.id.editTextUsuario);
        contrasena = findViewById(R.id.editTextPassword);
        btnBuscar = findViewById(R.id.buttonBuscar);
        btnGuardar = findViewById(R.id.buttonGuardar);

        btnGuardar.setOnClickListener(accionGuardar);
        btnBuscar.setOnClickListener(accionBuscar);




    }

    private void limpiar(){

        usuario.setText("");
        contrasena.setText("");

    }

    View.OnClickListener accionGuardar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Usuario user = Usuario.getUsuario(usuario.getText().toString());

            if (user.getNombre() != null){

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                alertDialog.setMessage("El usuario " + user.getCorreo() +" existe");

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        limpiar();
                    }
                });

                alertDialog.show();


            }else {

                user.setNombre("testNombre");
                user.setApellido("testApellido");
                user.setCorreo(usuario.getText().toString());
                user.setUser(user.getCorreo());
                user.setPass(contrasena.getText().toString());
                user.setUsuario_activo(true);
                user.save();

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                alertDialog.setMessage("Usuario Guardado");

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        limpiar();
                    }
                });

                alertDialog.show();
            }



        }
    };

    View.OnClickListener accionBuscar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Usuario user = Usuario.getUsuario(usuario.getText().toString());

            if (user.getNombre() != null){

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                alertDialog.setMessage("El usuario " + user.getCorreo() +" existe");

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        limpiar();
                    }
                });

                alertDialog.show();

//
            }else {

                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();

                alertDialog.setMessage(usuario.getText().toString() +"  no existe en la base de datos");

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        limpiar();
                    }
                });

                alertDialog.show();
            }

        }
    };
}
